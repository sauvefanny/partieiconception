# Description du projet
Créer une petite application de gestion de budget que vous allez utiliser dans le cadre de votre foyer.

## Fonctionnalités attendues

cf:voir le diagramme de useCase

  le nom des méthodes: choix du kamelCase, de l'anglais et de sa fonction première

  J'ai fait le choix que les entrees soit un budget mensuel afin de partir de données pouvant étre récurante. Ce budget est lié à un utilisateur afin de pouvoir dans une version suivante de filtrer par utilisateur. l'attribut month et année permettra de visualisé des evolutions dans le budget,il est indispensable d'avoir un debut et un fin. 


  la classe catégorie permettra d'avoir accès a des dépenses catégorisées, elles doivent pouvoir être modifié par des méthode CRUD.  

  l'attribut unchangeableSpent permettra de passer une entrée de modifiable a non modifiable.

